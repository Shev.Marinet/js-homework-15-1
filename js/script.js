const timerBox = document.createElement('div');
const startBut = document.createElement('button');
const pauseBut = document.createElement('button');
const clearBut = document.createElement('button');
let minutes = "00";
let seconds = "00";
let milliseconds = "00";
let timerId = null;
let start = null;
timerBox.id = 'timer-box';
timerBox.style.height = '50px';
timerBox.style.width = '132px';
timerBox.style.margin =' 30px auto';
timerBox.style.border = '5px solid rgb(97, 198, 157)';
timerBox.style.color = 'rgb(45, 114, 188)';
timerBox.style.fontSize ='36px';
document.body.appendChild(timerBox);
startBut.id = 'start';
startBut.style.display = 'inline-block';
startBut.style.width = '150px';
startBut.style.padding = '25px 0';
startBut.style.marginLeft = 'calc(50% - 200px)';
startBut.style.borderStyle = 'none';
startBut.style.borderRadius = '10px';
startBut.style.backgroundImage ='linear-gradient(to left, rgb(97, 198, 157) 0%, rgb(45, 114, 188) 100%)';
startBut.style.color = 'rgb(255,255,255)';
startBut.innerText = `Start`;
pauseBut.id = 'pause';
pauseBut.style.display = 'inline-block';
pauseBut.style.width = '150px';
pauseBut.style.padding = '25px 0';
pauseBut.style.marginLeft = 'calc(50% - 200px)';
pauseBut.style.borderStyle = 'none';
pauseBut.style.borderRadius = '10px';
pauseBut.style.backgroundImage ='linear-gradient(to left, rgb(97, 198, 157) 0%, rgb(45, 114, 188) 100%)';
pauseBut.style.color = 'rgb(255,255,255)';
pauseBut.style.textAlign = 'center';
pauseBut.innerText = `Pause`;
clearBut.id = 'clear';
clearBut.style.display = 'inline-block';
clearBut.style.padding = '25px 0';
clearBut.style.width = '150px';
clearBut.style.marginLeft = '100px';
clearBut.style.borderStyle = 'none';
clearBut.style.borderRadius = '10px';
clearBut.style.backgroundImage ='linear-gradient(to right, rgb(97, 198, 157) 0%, rgb(45, 114, 188) 100%)';
clearBut.style.color = 'rgb(255, 255, 255)';
clearBut.innerText = `Clear`;
document.body.appendChild(startBut);
document.body.appendChild(clearBut);
display = document.getElementById('timer-box');
display.textContent = minutes + ":" + seconds + "." + milliseconds;

function startTimer(display) {
    let speed = 10;
    let time =0;
    let diff =0;
    let start = new Date().getTime();
    timerId = setTimeout(function run(){
        let real = ((+milliseconds + (+seconds*100) + (+minutes*60*100)) * speed);
        let ideal = (new Date().getTime() - start);
        if (+minutes < 60){
            ++milliseconds;
            milliseconds = milliseconds < 10 ? "0" + milliseconds : milliseconds;
            if (milliseconds >= 100){
                ++seconds;
                milliseconds = "00";
                seconds = seconds < 10 ? "0" + seconds : seconds;
                if (seconds >= 60){
                    ++minutes;
                    seconds = "00";
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                }
            }
            diff = ideal - real;
            time = speed - diff;
            if (time >1000 || time < -1000){
                start = new Date().getTime()- timerId * speed;
                real = ((+milliseconds + (+seconds*100) + (+minutes*60*100)) * speed);
                ideal = (new Date().getTime() - start);
                diff = ideal - real;
                time = speed-diff;
            }
            display.textContent = minutes + ":" + seconds + "." + milliseconds;
            timerId = setTimeout(run, time);

        }
    }, time);

}

document.addEventListener('click', function (e) {
    if (e.target.id === 'start') {
        startTimer(display);
        document.body.removeChild(startBut);
        document.body.insertBefore(pauseBut, clearBut);
    }

    if (e.target.id === 'pause'){
        clearTimeout(timerId);
        pauseBut.parentElement.removeChild(pauseBut);
        document.body.insertBefore(startBut, clearBut);
    }

    if (e.target.id === 'clear' && timerId ){
        clearTimeout(timerId);
        if (document.getElementById('pause')){
            pauseBut.parentElement.removeChild(pauseBut);
            document.body.insertBefore(startBut, clearBut);
        }
        minutes = "00";
        seconds = "00";
        milliseconds = "00";
        display.textContent = minutes + ":" + seconds + "." + milliseconds;
    }
});
